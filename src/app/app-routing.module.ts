import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard, AuthGuardLogin } from './auth/guards/auth.guard';
import { LoginComponent } from './auth/login/login.component';

const routes: Routes = [
  
  { 
    path: 'login',
    component:LoginComponent,
    canActivate: [ AuthGuardLogin ]
  },
  { 
    path: 'configuraciones', 
    loadChildren:  () => import ('./configuraciones/configuraciones.module').then (r => r.ConfiguracionesModule),
    canActivate: [ AuthGuard ]
  },

  { 
    path: 'adminitration', 
    loadChildren:  () => import ('./administration/administration.module').then (r => r.AdministrationModule)    
  },
  
  { 
    path: 'core',
    loadChildren:  () => import ('./core/core.module').then (r => r.CoreModule)
  },
  { 
    path: '**',
    redirectTo: 'login',
    pathMatch: 'full'
  },
 

];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash:true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
