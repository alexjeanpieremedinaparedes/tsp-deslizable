
import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { MatSidenav } from '@angular/material/sidenav';
import { Envsystem } from 'src/app/core/model/env.model';
import { FunctionsService } from 'src/app/core/service/functions.service';
import { IAuthResult } from 'src/app/core/interface/auth.interface';
import { AuthService } from 'src/app/auth/service/auth.service';
import { FormControl } from '@angular/forms';
import { SettingService } from 'src/app/configuraciones/service/setting.service';
import { Router } from '@angular/router';

export interface NavItem {
  displayName: string;
  disabled?: boolean;
  iconName: string;
  estado?: boolean;
  route?: string;
  children?: NavItem[];
}

@Component({
  selector: 'app-siderbar',
  templateUrl: './siderbar.component.html',
  styleUrls: ['./siderbar.component.css']
})

export class SiderbarComponent implements OnInit {

  public dataLogin?: IAuthResult;
  public menutoogle?: boolean;
  public loading?: boolean;
  public isAuthenticated?: boolean;
  public title?: string;
  public isBypass?: boolean;
  public mobile?: boolean;
  public isMenuInitOpen?: boolean;
  public isMenuOpen: boolean = true;
  public contentMargin: number = 240;

  private sidenav?: MatSidenav;
  private env = new Envsystem();
  public search = new FormControl('');

  constructor(
    private breakpointObserver: BreakpointObserver,
    private sfn: FunctionsService,
    private sauth: AuthService,
    private ssetting: SettingService,
    private router: Router
  ) {
    this.loadData();
  }

  ngOnInit() {
    this.isMenuOpen = true; // Open side menu by default
    this.title = 'FactuTED';
    this.search.valueChanges.subscribe( value => {

      const body = {
        url: this.router.url,
        value: value
      };

      this.ssetting.textSearch$.emit(body);
    });
  }

  private loadData() {
    const encrypt = localStorage.getItem(this.env.dataLogin) ?? '';
    const decrypt = this.sfn.decrypt(encrypt);
    this.dataLogin = decrypt;
  }

  public async logout() {

    this.menutoogle = false;
    const head1 = 'Desea';
    const head2 = 'cerrar';
    const head3 = 'Sesion';
    const message = 'Se cerrara la sesion del usuario';
    const contine = await this.sfn.modalQuestion(head1, head2, head3, message, true);
    if(contine) {
      this.sauth.logout();
    }
  }

  get isHandset(): boolean {
    return this.breakpointObserver.isMatched(Breakpoints.Handset);
  }

  // *********************************************************************************************
  // * LIFE CYCLE EVENT FUNCTIONS
  // *********************************************************************************************

  ngDoCheck() {
    if (this.isHandset) {
      this.isMenuOpen = false;
    } else {
      this.isMenuOpen = true;
    }
  }

  // *********************************************************************************************
  // * COMPONENT FUNCTIONS
  // *********************************************************************************************

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  setStep(item: NavItem) {
    this.menu.forEach(el => el.estado = false);
    item.estado = true;
  }

  // *********************************************************************************************
  // * OBJECT MENU
  // *********************************************************************************************

  menu: NavItem[] = [

    {
      displayName: 'Venta',
      iconName: 'assets/svg/venta.svg',
      route: 'entradasGADE',
    },

    {
      displayName: 'Embarque',
      iconName: 'assets/svg/embarque.svg',
      route: 'entradasGADE',
    },
   
    {
      displayName: 'Administración',
      iconName: 'assets/svg/administracion.svg',
      estado: true,
      route: '/adminitration',
      children: [
        {
          displayName: 'Personal Operativo',
          iconName: 'assets/svg/submenu.svg',
          route: '/adminitration/operationalStaff',

        },
        {
          displayName: 'Registro Bus',
          iconName: 'assets/svg/submenu.svg',
          route: 'dsdgsdgsdg',

        },
        
      ]
    },

    {
      displayName: 'Configuraciones',
      iconName: 'assets/svg/configuraciones.svg',
      estado: false,
      route: '/configuraciones',
      children: [
        {
          displayName: 'Sucursales',
          iconName: 'assets/svg/submenu.svg',
          route: '/configuraciones/sucursales',

        },
        {
          displayName: 'Usuarios',
          iconName: 'assets/svg/submenu.svg',
          route: '/planilla/nomina',

        },
        {
          displayName: 'Talonario',
          iconName: 'assets/svg/submenu.svg',
          route: '/planilla/nomina',
          

        },
        {
          displayName: 'Más Opciones',
          iconName: 'assets/svg/submenu.svg',
          route: '/planilla/nomina',

        },
      ]
    },
  ];
}
