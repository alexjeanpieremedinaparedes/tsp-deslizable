import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdministrationRoutingModule } from './administration-routing.module';
import { SeeDetailComponent } from './pages/operationalStaff/see-detail/see-detail.component';
import { NewStaffComponent } from './pages/operationalStaff/new-staff/new-staff.component';
import { FilterDataComponent } from './pages/operationalStaff/filter-data/filter-data.component';
import { TableStaffComponent } from './pages/operationalStaff/table-staff/table-staff.component';
import { RouterModule } from '@angular/router';
import { MaterialModule } from '../materia-module';
import { HttpClientModule } from '@angular/common/http';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { UserProfileComponent } from './pages/operationalStaff/user-profile/user-profile.component';
import { NewUserProfileComponent } from './pages/operationalStaff/new-user-profile/new-user-profile.component';


@NgModule({
  declarations: [    
    SeeDetailComponent,   
    NewStaffComponent,
    FilterDataComponent,
    TableStaffComponent,
    UserProfileComponent,
    NewUserProfileComponent
  ],
  
  imports: [
    CommonModule,
    AdministrationRoutingModule,
    RouterModule,
    MaterialModule,
    HttpClientModule,
    AngularSvgIconModule.forRoot(),
  ]
})
export class AdministrationModule { }
