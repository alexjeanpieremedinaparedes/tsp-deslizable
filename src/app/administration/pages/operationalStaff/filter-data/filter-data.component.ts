import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-filter-data',
  templateUrl: './filter-data.component.html',
  styleUrls: ['./filter-data.component.css']
})
export class FilterDataComponent implements OnInit {


  @Output() closeForm  = new EventEmitter<boolean>();
  @Output() responseok = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit(): void {
  }

  public closeFrm() {
    this.closeForm.emit(false);
  }

}
