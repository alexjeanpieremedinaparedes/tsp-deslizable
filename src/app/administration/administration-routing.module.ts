import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from '../layouts/dashboard/dashboard.component';
import { TableStaffComponent } from './pages/operationalStaff/table-staff/table-staff.component';

const routes: Routes = [

  {
    path: '',
    component:  DashboardComponent,
     children : [
       {path: 'operationalStaff', component : TableStaffComponent},
       {path: '**', redirectTo: 'operationalStaff'}
     ]

  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdministrationRoutingModule { }
