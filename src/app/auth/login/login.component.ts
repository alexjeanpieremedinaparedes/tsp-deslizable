import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Envsystem } from 'src/app/core/model/env.model';
import { FunctionsService } from 'src/app/core/service/functions.service';
import { ValidationService } from 'src/app/core/service/validation.service';
import { AuthService } from '../service/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

  private env = new Envsystem();
  public form!: FormGroup;

  constructor(
    public sfn: FunctionsService,
    private sauth: AuthService,
    private router: Router,
    private fb: FormBuilder,
    private sval: ValidationService,
    private spinner: NgxSpinnerService
    ) {
      this.createForm();
    }

  ngOnInit(): void {
  }

  get rucInvalid() {
    return this.sval.ctrInvalid('ruc', this.form);
  }

  get userInvalid() {
    return this.sval.ctrInvalid('usuario', this.form);
  }

  get passInvalid() {
    return this.sval.ctrInvalid('password', this.form);
  }

  private createForm() {
    this.form = this.fb.group({
      ruc     : [ '', [ Validators.required,  Validators.minLength(11), Validators.maxLength(11) ] ],
      usuario : [ '', [ Validators.required ] ],
      password: [ '', [ Validators.required ] ]
    });
  }

  public login() {

    if(this.form.invalid) {
      this.sfn.onFocus('rucLogin');
      return this.sval.emptyData(this.form);
    }

    this.spinner.show();
    const body = {
      ... this.form.value
    };

    this.sauth.login(body).subscribe( response => {
      const ok = response.status === 200;
      if(ok) {
        const info = JSON.stringify(response.result);
        localStorage.setItem(this.env.dataLogin, this.sfn.encrypt(info));
        this.continuePage();
      }

      this.spinner.hide();
    }, (e) => {

      this.spinner.hide();
      this.sfn.onFocus('rucLogin');
      this.sfn.except(e);
    })
  }

  private continuePage() {
    this.router.navigateByUrl('/adminitration', { replaceUrl: true });
  }

}
