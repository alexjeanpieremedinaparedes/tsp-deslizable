import { Injectable } from '@angular/core';
import { CanActivate, Router, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { Envsystem } from 'src/app/core/model/env.model';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  
  private env = new Envsystem();
  constructor(private router:Router) { }

  canActivate( ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    const sessionActive = localStorage.getItem(this.env.dataLogin);
    if(!sessionActive) {
      this.router.navigateByUrl('/login', { replaceUrl: true });
    }
    return true;
  }
  
}

@Injectable({
  providedIn: 'root'
})
export class AuthGuardLogin implements CanActivate {
  
  private env = new Envsystem();
  constructor(private router:Router) { }

  canActivate( ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    const sessionActive = localStorage.getItem(this.env.dataLogin);
    if(sessionActive) {
      this.router.navigateByUrl('/configuraciones', { replaceUrl: true });
    }
    return true;
  }
  
}