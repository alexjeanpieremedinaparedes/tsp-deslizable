import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CoreRoutingModule } from './core-routing.module';

import { NgxSpinnerModule } from "ngx-spinner";
import { LoadingComponent } from './components/loading/loading.component';
import { MaterialModule } from '../materia-module';
import { NotificationComponent } from './components/notification/notification.component';
import { QuestionComponent } from './components/question/question.component';
import { ErrorComponent } from './components/error/error.component';
import { PhotoPersonalForDefaultDirective } from './directive/photo-personal-for-default.directive';
import { SearchMuiltiplePipe } from './pipe/search-muiltiple.pipe';


@NgModule({
  declarations: [
    LoadingComponent,
    NotificationComponent,
    QuestionComponent,
    ErrorComponent,
    PhotoPersonalForDefaultDirective,
    SearchMuiltiplePipe
  ],
  exports:[
    LoadingComponent,
    QuestionComponent,
    PhotoPersonalForDefaultDirective,
    SearchMuiltiplePipe
  ],
  imports: [
    CommonModule,
    CoreRoutingModule,
    NgxSpinnerModule,
    MaterialModule
  ]
})
export class CoreModule { }
