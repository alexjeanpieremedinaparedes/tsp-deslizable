import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchMuiltiple'
})
export class SearchMuiltiplePipe implements PipeTransform {

  transform(list: any[], text: string): any[] {
    
    let arrBusqueda = text.split(",");
    if(!text) return list;
    return list.filter((item)=>{ 
                 let a =  arrBusqueda.map((value)=> JSON.stringify(item).toUpperCase().indexOf(value.toUpperCase()) > -1 ? 1 : 0 )[0];
                 return a > 0
                });
  }

}
