import { Directive, ElementRef, HostListener } from '@angular/core';
import { Envsystem } from '../model/env.model';

@Directive({
  selector: '[appPhotoPersonalForDefault]'
})
export class PhotoPersonalForDefaultDirective {

  private env = new Envsystem();
  constructor( private elementImg : ElementRef ) { }

  @HostListener('error')
  onError() : void{
    this.elementImg.nativeElement.src = this.env.photoPersonalForDefault;
  }

}
