import { environment } from "src/environments/environment";

export class Envsystem {
    mainUrl: string;
    photoPersonalForDefault: string;
    dataLogin: string;
    expiredms: string;
    brokenConnectionms: string;
    keyLocalStorage: string;

    constructor() {
        this.mainUrl = environment.mainUrl;
        this.dataLogin = '_innv.datalogintsp';
        this.expiredms = 'conexión expirada, vuelva a iniciar sesión';
        this.brokenConnectionms = 'Sin conexión al servidor';
        this.photoPersonalForDefault = 'assets/img/default/photoForDefault.jpg';
        this.keyLocalStorage = 'tsp';
    }
}