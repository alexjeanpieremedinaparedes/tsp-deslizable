import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FunctionsService } from '../../service/functions.service';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit {

  constructor( 
    private dialogRef: MatDialogRef<FunctionsService>,
    @Inject( MAT_DIALOG_DATA ) public data: any
   ) { }

  ngOnInit(): void {
  }

  continue() {
    this.dialogRef.close({ exito: true });
  }


}
