import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AuthService } from 'src/app/auth/service/auth.service';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.css']
})
export class ErrorComponent implements OnInit {

  constructor( 
    @Inject(MAT_DIALOG_DATA) public data:any,
    private sauth: AuthService ) { }

  ngOnInit(): void {
  }

  continue() {
    if( this.data.expired ) {
      this.sauth.logout();
    }
  }
}
