export interface Departamento {
    IDDepartamento: number;
    departamento:   string;
    provincia:      Provincia[];
}

export interface Provincia {
    iddepartamento: number;
    idprovincia:    number;
    provincia:      string;
    distrito:      Distrito[];
}

export interface Distrito {
    idprovincia: number;
    iddistrito:  number;
    distrito:    string;
}

export interface TypeOffice {
    id: number;
    name: string;
    alias: string;
    status: boolean;
}