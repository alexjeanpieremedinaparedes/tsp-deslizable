export interface IAuth {
    status:  number;
    message: string;
    result:  IAuthResult;
}

export interface IAuthResult {
    idusuario:       number;
    usuario:         string;
    estado:          boolean;
    empleado:        string;
    numerodocumento: string;
    direccion:       string;
    fotourl:         string;
    cargo:           string;
    token:           string;
}
