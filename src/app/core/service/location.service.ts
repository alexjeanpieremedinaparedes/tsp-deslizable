import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Departamento, TypeOffice } from '../interface/location.interface';
import { Envsystem } from '../model/env.model';
import { FunctionsService } from './functions.service';

@Injectable({
  providedIn: 'root'
})
export class LocationService {

  private env = new Envsystem();
  private file: string;
  constructor(
    private http: HttpClient,
    private sfn: FunctionsService
  ) {
    this.file = 'ubicacion/';
  }

  public getLocation(): Observable<Departamento[]> {
    const headers = this.sfn.heardToken();
    return this.http.get<Departamento[]>(`${this.env.mainUrl}${this.file}listardepartamento`, { headers }).pipe( map( (response: any) => response.result ) );
  }

  public getTypeOffice() {
    return this.http.get<TypeOffice[]>('assets/json/typeOffice.json');
  }

}
