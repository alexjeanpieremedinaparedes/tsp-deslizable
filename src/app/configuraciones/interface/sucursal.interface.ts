
export interface ISucursal {
    idsucursal:         number;
    iddepartamento:     number;
    idprovincia:        number;
    iddistrito:         number;
    departamento:       string;
    provincia:          string;
    distrito:           string;
    sucursal:           string;
    direccion:          string;
    codigoanexo:        string;
    puntoventa:         string;
    paradero:           string;
    paraderointermedio: string;
    comisionista:       boolean;
    porcentajecomision: number;
    color:              string;
    estado:             boolean;
}
