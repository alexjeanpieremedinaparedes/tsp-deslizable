import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from '../layouts/dashboard/dashboard.component';
import { SucursalComponent } from './pages/sucursales/sucursal/sucursal.component';


const routes: Routes = [
  {
    path: '',
    component:  DashboardComponent,
     children : [
       {path: 'sucursales', component : SucursalComponent},
       {path: '**', redirectTo: 'sucursales'}
     ]

  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfiguracionesRoutingModule { }
