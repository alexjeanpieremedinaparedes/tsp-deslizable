import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ISucursal } from 'src/app/configuraciones/interface/sucursal.interface';
import { FilterOffice } from 'src/app/configuraciones/model/filter.model';
import { SettingService } from 'src/app/configuraciones/service/setting.service';
import { SucursalService } from 'src/app/configuraciones/service/sucursal.service';
import { FunctionsService } from 'src/app/core/service/functions.service';
import { DetallesucursalComponent } from '../detallesucursal/detallesucursal.component';

@Component({
  selector: 'app-sucursal',
  templateUrl: './sucursal.component.html',
  styleUrls: ['./sucursal.component.css']
})
export class SucursalComponent implements AfterViewInit, OnInit {

  public filterOffice = new FilterOffice();
  public openOffice: boolean = false;
  public openFilter: boolean = false;
  public showPaginator: boolean = false;
  public body: any;
  public bodyFilter?: FilterOffice;

  public displayedColumns: string[] = ['Nombre Sucursal', 'Dirección', 'Especificación sucursal', 'Comisionista', 'Estado', 'opciones' ];
  public dataSource = new MatTableDataSource<ISucursal>([]);

  private dataSourceCopy: ISucursal[] = [];
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  constructor(
    private modals:MatDialog,
    private sOffice: SucursalService,
    private spinner: NgxSpinnerService,
    private sfn: FunctionsService,
    private ssetting: SettingService,
    private router: Router
  ) {
    this.getOffce();
  }

  ngOnInit(): void {

    this.ssetting.textSearch$.subscribe( response => {

      const urlActual = this.router.url;
      const urlReceived = response.url;
      if( urlActual === urlReceived ) {
        this.dataSource.filter = response.value;
      }
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  public getOffce() {
    
    this.spinner.show();
    this.sOffice.getSucursal().subscribe( response => {

      this.dataSourceCopy = response;
      this.dataSource = new MatTableDataSource<ISucursal>(response);
      this.dataSource.paginator = this.paginator;
      this.showPaginator = this.dataSource.data.length > 10;
      if(this.showPaginator) this.paginator._intl.itemsPerPageLabel = 'items por pagina';
      this.spinner.hide();
    }, (e) => {
      this.spinner.hide();
      this.sfn.except(e);
    });
  }

  public seeDetail(item: ISucursal) {

    this.modals.open(DetallesucursalComponent, {
      data: item,
      disableClose: true
    });
  }

  public async activeOffice( item: ISucursal ) {

    const status = item.estado ? 'Desactivar' : 'Activar';
    const statusms = item.estado ? 'desactivara' : 'activara';
    const danger = item.estado;
    const head1 = 'Desea';
    const head2 = status;
    const head3 = 'la Sucursal';
    const message = `Se ${statusms} la sucursal`;
    const contine = await this.sfn.modalQuestion(head1, head2, head3, message, danger);
    if(contine) {
      
      this.spinner.show();
      const body = {
        idsucursal: item.idsucursal
      }

      this.sOffice.changeStatusSucursal(body).subscribe( (response: any) => {

        const ok = response.status === 200;
        if(ok) {
          const result = response.result;
          const status = result.estado ? 'Activado' : 'Desactivado';
          this.sfn.openSnackBar(`Sucursal ${status}`);
          this.getOffce();
        }
        this.spinner.hide();
      }, (e) => {
        this.spinner.hide();
        this.sfn.except(e);
      });
    }
  }

  public async deleteOffice( item: ISucursal ) {
    const head1 = 'Desea';
    const head2 = 'eliminar';
    const head3 = 'la Sucursal';
    const message = 'Se eliminara la sucursal';
    const contine = await this.sfn.modalQuestion(head1, head2, head3, message, true);
    if(contine) {
      
      this.spinner.show();
      const body = { idsucursal: item.idsucursal };
      this.sOffice.deleteSucursal(body).subscribe( (response: any) => {

        const ok = response.status === 200;
        if(ok) {
          this.sfn.openSnackBar('Sucursal eliminado con exito');
          this.getOffce();
        }
        this.spinner.hide();
      }, (e) => {
        this.spinner.hide();
        this.sfn.except(e);
      });
    }
  }

  public editOffice( edit: boolean, item: any = null  ) {

    this.openOffice = true;
    this.body = {
      edit: edit,
      status: edit ? 'Editar' : 'Nueva',
      item: item
    }
  }

  public openFilterAndSendData() {

    this.openFilter = true;
    this.bodyFilter = {
      ... this.filterOffice      
    };
  }

  public responseFilter($event: any) {

    this.filterOffice = $event;
    const filter: any[] = [];
    const array = Object.entries(this.filterOffice);
    array.forEach( el =>{ if( el[1] ) filter.push(el[0]) });
    this.dataSource = new MatTableDataSource<ISucursal>(this.dataSourceCopy);
    if(filter.length > 0) {

      this.dataSource = new MatTableDataSource<ISucursal>(this.dataSource.data.filter( result => {
  
        if( filter.includes('puntoventa') && result.puntoventa !== '' ) {
          return result;
        }
        else if( filter.includes('paradero') && result.paradero !== '' ) {
          return result;
        }
        else if( filter.includes('paraderoIntermedio') && result.paraderointermedio !== '' ){
          return result;
        }
        else if( filter.includes('comisionista') && result.comisionista ) {
          return result;
        }
        else if( filter.includes('nocomisionista') && !result.comisionista ) {
          return result;
        }
        else if( filter.includes('activo') && result.estado ) {
          return result;
        }
        else if( filter.includes('inactivo') && !result.estado ) {
          return result;
        }
  
        return;
      }));
    }
    
    this.dataSource.paginator = this.paginator
  }
}
