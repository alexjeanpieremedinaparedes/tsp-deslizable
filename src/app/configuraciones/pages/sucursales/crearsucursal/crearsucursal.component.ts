import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { SucursalService } from 'src/app/configuraciones/service/sucursal.service';
import { Departamento, Distrito, Provincia, TypeOffice } from 'src/app/core/interface/location.interface';
import { FunctionsService } from 'src/app/core/service/functions.service';
import { LocationService } from 'src/app/core/service/location.service';
import { ValidationService } from 'src/app/core/service/validation.service';
import { Observable } from 'rxjs';
import { ISucursal } from 'src/app/configuraciones/interface/sucursal.interface';

@Component({
  selector: 'app-crearsucursal',
  templateUrl: './crearsucursal.component.html',
  styleUrls: ['./crearsucursal.component.css']
})
export class CrearsucursalComponent implements OnInit {

  @Input() body: any;
  @Output() closeForm  = new EventEmitter<boolean>();
  @Output() responseok  = new EventEmitter<boolean>();

  form!: FormGroup;
  listLocation: Departamento[] = [];
  listProvince: Provincia[] = [];
  listDistrite: Distrito[] = [];
  listTypeOffice: Observable<TypeOffice[]>;

  constructor(
    public sfn: FunctionsService,
    private fb: FormBuilder,
    private sval: ValidationService,
    private sOffice: SucursalService,
    private sLocation: LocationService,
    private spinner: NgxSpinnerService
  ) {
    this.getLocation();
    this.createForm();
    this.listTypeOffice = this.sLocation.getTypeOffice();
  }

  ngOnChanges(changes: SimpleChanges): void {
    const edit = changes['body'].currentValue.edit;
    const item: ISucursal = changes['body'].currentValue.item;
    if(edit) {
      this.editForm(item);
    }
  }

  ngOnInit(): void {
  }

  getLocation() {

    this.spinner.show();
    this.sLocation.getLocation().subscribe( response => {

      this.listLocation = response;
      const idDepartament = this.form.value.iddepartamento;
      const idProvince = this.form.value.idprovincia;
      if(idDepartament) this.getProvince(idDepartament);
      if(idProvince) this.getDistrite(idProvince);
      this.spinner.hide();
    }, (e) => {
      this.spinner.hide();
      this.sfn.except(e);
    })
  }

  getProvince( idDepartamento: number ) {
    this.listProvince = this.listLocation.filter( x=>x.IDDepartamento === idDepartamento ).map(x=>x.provincia)[0];
  }

  getDistrite( idProvincia: number ) {
    this.listDistrite = this.listProvince.filter( x=>x.idprovincia === idProvincia ).map(x=>x.distrito)[0];
  }

  get officeInvalid() {
    return this.sval.ctrInvalid('sucursal', this.form);
  }

  get iddepartamentoInvalid() {
    return this.sval.ctrInvalid('iddepartamento', this.form);
  }

  get idprovinciaInvalid() {
    return this.sval.ctrInvalid('idprovincia', this.form);
  }

  get iddistritoInvalid() {
    return this.sval.ctrInvalid('iddistrito', this.form);
  }

  get direccionInvalid() {
    return this.sval.ctrInvalid('direccion', this.form);
  }

  get codAnexoInvalid() {
    return this.sval.ctrInvalid('codigoanexo', this.form);
  }

  get porcentajeInvalid() {
    return this.sval.ctrInvalid('porcentajecomision', this.form);
  }

  get colorInvalid() {
    return this.sval.ctrInvalid('color', this.form);
  }

  createForm() {
    this.form = this.fb.group({
      idsucursal        : [ 0 ],
      iddepartamento    : [ '', [ Validators.required ] ],
      idprovincia       : [ '', [ Validators.required ] ],
      iddistrito        : [ '', [ Validators.required ] ],
      sucursal          : [ '', [ Validators.required ] ],
      direccion         : [ '', [ Validators.required ] ],
      codigoanexo       : [ '', [ Validators.required, Validators.minLength(4), Validators.maxLength(4) ] ],
      color             : [ '#f20707', [ Validators.required ] ],
      porcentajecomision: [ 0 ],
      puntoventa        : [ false ],
      paradero          : [ false ],
      paraderointermedio: [ false ],
      comisionista      : [ false ],
      estado            : [ true  ],
      editar            : [ false ]
    });
  }

  editForm(item: ISucursal) {

    this.form.patchValue({
      idsucursal        : item.idsucursal,
      iddepartamento    : item.iddepartamento,
      idprovincia       : item.idprovincia,
      iddistrito        : item.iddistrito,
      sucursal          : item.sucursal,
      direccion         : item.direccion,
      codigoanexo       : item.codigoanexo,
      color             : item.color,
      porcentajecomision: item.porcentajecomision ?? 0,
      puntoventa        : item.puntoventa === '' ? false: true,
      paradero          : item.paradero === '' ? false: true,
      paraderointermedio: item.paraderointermedio === '' ? false: true,
      comisionista      : item.comisionista,
      estado            : item.estado,
      editar            : true
    });
  }

  closeFrm() {
    this.closeForm.emit(false);
  }

  changeTypeOffice(item: TypeOffice, checked: boolean) {
    this.form.patchValue({ [item.alias]: checked })
  }

  validateCommissionAgent(checked: boolean) {

    this.form.get('porcentajecomision')?.clearValidators();
    this.form.get('porcentajecomision')?.setValue(0);
    if(checked) this.form.get('porcentajecomision')?.setValidators([Validators.required, Validators.maxLength(3)]);
    this.form.get('porcentajecomision')?.updateValueAndValidity();
  }

  saveSucursal() {

    if(this.form.invalid) {
      return this.sval.emptyData(this.form);
    }

    this.spinner.show();
    const body = {
      ... this.form.value
    }

    body.porcentajecomision = Number(body.porcentajecomision);
    delete body.iddepartamento;
    delete body.idprovincia;

    this.sOffice.setSucursal( body ).subscribe( (response: any)=> {

      const ok = response.status === 200;
      if(ok) {
        this.clearForm();
        const status = this.body.edit ? 'editado' : 'agregado';
        this.sfn.openSnackBar(`Sucursal ${status} con exito`);
        this.responseok.emit(true);
        if(this.body.edit) {
          this.closeFrm();
        }
      }
      this.spinner.hide();
    }, (e) => {
      this.spinner.hide();
      this.sfn.except(e);
    });
  }

  clearForm() {
    this.listTypeOffice = this.sLocation.getTypeOffice();
    this.form.reset();
    this.form.patchValue({
      idsucursal        : 0,
      iddepartamento    : '',
      idprovincia       : '',
      iddistrito        : '',
      sucursal          : '',
      direccion         : '',
      codigoanexo       : '',
      color             : '#f20707',
      porcentajecomision: 0,
      puntoventa        : false,
      paradero          : false,
      paraderointermedio: false,
      comisionista      : false,
      estado            : true,
      editar            : false
    });
  }

  validatePorcentual(value: string) {

    const ok = this.sfn.validatePorcentual(value);
    if(!ok) {

      this.form.controls['porcentajecomision'].setErrors({'incorrect': true});
      this.form.controls['porcentajecomision'].markAsTouched();
    }
  }

}