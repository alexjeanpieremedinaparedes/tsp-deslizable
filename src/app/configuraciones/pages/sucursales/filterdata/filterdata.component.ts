import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { FilterOffice } from '../../../model/filter.model';

@Component({
  selector: 'app-filterdata',
  templateUrl: './filterdata.component.html',
  styleUrls: ['./filterdata.component.css']
})
export class FilterdataComponent implements OnInit {
  
  @Input() receiveData?: FilterOffice;
  @Output() closeForm  = new EventEmitter<boolean>();
  @Output() responseok = new EventEmitter<boolean>();
  form!: FormGroup;

  constructor( private fb: FormBuilder ) { }

  ngOnChanges(changes: SimpleChanges): void {
    
    const data: FilterOffice = changes['receiveData'].currentValue;
    this.createForm(data);
  }

  ngOnInit(): void {
  }

  private createForm(data: FilterOffice) {

    this.form = this.fb.group({
      puntoventa        : [ data.puntoventa ],
      paradero          : [ data.paradero ],
      paraderoIntermedio: [ data.paraderoIntermedio ],
      comisionista      : [ data.comisionista ],
      nocomisionista    : [ data.nocomisionista ],
      activo            : [ data.activo ],
      inactivo          : [ data.inactivo ]
    });
  }

  public closeFrm() {
    this.closeForm.emit(false);
  }

  public resetFilter() {

    this.form.patchValue({
      puntoventa: false,
      paradero: false,
      paraderoIntermedio: false,
      comisionista: false,
      nocomisionista: false,
      activo: false,
      inactivo: false
    });
  }

  continue() {
    const filter = this.form.value;
    this.responseok.emit(filter);
    this.closeFrm();
  }

}
