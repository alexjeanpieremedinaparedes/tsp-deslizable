import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ISucursal } from 'src/app/configuraciones/interface/sucursal.interface';

@Component({
  selector: 'app-detallesucursal',
  templateUrl: './detallesucursal.component.html',
  styleUrls: ['./detallesucursal.component.css']
})
export class DetallesucursalComponent implements OnInit {

  constructor(
    @Inject( MAT_DIALOG_DATA ) public data: ISucursal
  ) { }

  ngOnInit(): void {
  }

}
