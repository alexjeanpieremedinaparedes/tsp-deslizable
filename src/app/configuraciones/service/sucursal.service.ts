import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Envsystem } from 'src/app/core/model/env.model';
import { FunctionsService } from 'src/app/core/service/functions.service';
import { ISucursal } from '../interface/sucursal.interface';

@Injectable({
  providedIn: 'root'
})
export class SucursalService {

  private env = new Envsystem();
  private file : string;
  
  constructor( 
    private http: HttpClient,
    private sfn: FunctionsService
  ) {
    this.file = 'sucursal/';
  }

  public getSucursal(): Observable<ISucursal[]> {

    const headers = this.sfn.heardToken();
    return this.http.get<ISucursal[]>(`${this.env.mainUrl}${this.file}listarsucursal`, { headers }).pipe( map( (result: any) => result['result'] ) )
  }

  public changeStatusSucursal( body: any ) {

    const headers = this.sfn.heardToken();
    return this.http.post(`${this.env.mainUrl}${this.file}activardesactivarsucursal`, body,  { headers });
  }

  public deleteSucursal( body: any ) {

    const headers = this.sfn.heardToken();
    return this.http.post(`${this.env.mainUrl}${this.file}eliminarsucursal`, body, { headers });
  }

  public setSucursal( body: any ) {

    const headers = this.sfn.heardToken();
    return this.http.post(`${this.env.mainUrl}${this.file}agregarsucursal`, body, { headers });
  }
}
