import { EventEmitter, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SettingService {

  public textSearch$ = new EventEmitter<any>();
  constructor() { }
}
