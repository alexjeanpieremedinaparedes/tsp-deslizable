import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConfiguracionesRoutingModule } from './configuraciones-routing.module';
import { DashboardComponent } from '../layouts/dashboard/dashboard.component';
import { RouterModule } from '@angular/router';
import { SucursalComponent } from './pages/sucursales/sucursal/sucursal.component';
import { CrearsucursalComponent } from './pages/sucursales/crearsucursal/crearsucursal.component';
import { DetallesucursalComponent } from './pages/sucursales/detallesucursal/detallesucursal.component';
import { SharedModule } from '../shared/shared.module';
import { HttpClientModule } from '@angular/common/http';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { MaterialModule } from '../materia-module';
import { CoreModule } from '../core/core.module';
import { ReactiveFormsModule } from '@angular/forms';
import { FilterdataComponent } from './pages/sucursales/filterdata/filterdata.component';

@NgModule({
  declarations: [
    DashboardComponent,
    SucursalComponent,
    CrearsucursalComponent,
    DetallesucursalComponent,
    FilterdataComponent
  ],
  imports: [
    CommonModule,
    ConfiguracionesRoutingModule,
    RouterModule,
    SharedModule,
    HttpClientModule,
    AngularSvgIconModule.forRoot(),
    MaterialModule,
    CoreModule,
    ReactiveFormsModule,
    MaterialModule
  ]
})
export class ConfiguracionesModule { }
